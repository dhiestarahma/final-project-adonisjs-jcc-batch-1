import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Fields extends BaseSchema {
  //Release 0 - Membuat Migration untuk table Fields (Lapangan)
  protected tableName = 'fields'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name').notNullable()
      table.enum('type', ['futsal', 'soccer', 'volleyball' , 'minisoccer', 'basketball']).notNullable()
      table.integer('venue_id').unsigned()
      table.foreign('venue_id').references('venues.id')
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
