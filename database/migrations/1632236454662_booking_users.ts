import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BookingUsers extends BaseSchema {
  protected tableName = 'booking_user'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id').unsigned()
      table.foreign('user_id').references('users.id').onDelete('CASCADE')
      table.integer('booking_id').unsigned()
      table.foreign('booking_id').references('bookings.id').onDelete('CASCADE')
      table.unique(['user_id', 'booking_id'])
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
