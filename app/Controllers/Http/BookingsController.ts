import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field'
import User from 'App/Models/User'

/**
 * @swagger
 * /api/v1/fields/{field_id}/bookings:
 *  get:
 *      summary: Menampilkan seluruh data Booking
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      parameters:
 *          - name: field_id
 *            in: path
 *            description: field_id
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '200':
 *              description: berhasil mendapat seluruh data booking
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#definitions/Booking'
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 * 
 *  post:
 *      summary: melakukan booking
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      parameters:
 *          - name: field_id
 *            in: path
 *            description: field_id
 *            required: true
 *            schema:
 *              type: integer
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          play_date_start:
 *                              type: string
 *                              format: date-time
 *                              description: tanggal dan jam mulai main
 *                          play_date_end:
 *                              type: string
 *                              format: date-time
 *                              description: tanggal dan jam mulai main
 *                          title:
 *                              type: string
 *                              description: judul booking
 *                      required:
 *                          - play_date_start
 *                          - play_date_end
 *                          - title
 *      responses:
 *          '201':
 *              description: sukses melakukan booking
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '400':
 *              description: request body yang diinput tidak valid.
 * 
 * /api/v1/fields/{field_id}/bookings/{id}:
 *  get:
 *      security:
 *          - bearerAuth: []
 *      summary: menampilkan booking by id (beserta daftar dan jumlah player)
 *      tags:
 *          - Bookings
 *      parameters:
 *          - name: field_id
 *            in: path
 *            description: field_id
 *            required: true
 *            schema:
 *              type: integer
 *          - name: id
 *            in: path
 *            description: booking_id
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '200':
 *              description: berhasil mendapat data booking by id
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#definitions/Booking'
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '400':
 *              description: Not Found. Tidak ada data venue dengan id tersebut.
 * 
 *  put:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      summary: update booking by id
 *      parameters:
 *          - name: field_id
 *            in: path
 *            description: field_id
 *            required: true
 *            schema:
 *              type: integer
 *          - name: id
 *            in: path
 *            description: booking_id
 *            required: true
 *            schema:
 *              type: integer
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          play_date_start:
 *                              type: string
 *                              format: date-time
 *                              description: tanggal dan jam mulai main
 *                          play_date_end:
 *                              type: string
 *                              format: date-time
 *                              description: tanggal dan jam mulai main
 *                          title:
 *                              type: string
 *                              description: judul booking
 *                      required:
 *                          - play_date_start
 *                          - play_date_end
 *                          - title
 *      responses:
 *          '200':
 *              description: sukses mengupdate data booking
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '400':
 *              description: request body yang diinput tidak valid.
 *          '404':
 *              description: Not Found. Tidak ada data booking dengan id tersebut.
 * 
 *  delete:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      summary: hapus data booking by id
 *      parameters:
 *          - name: field_id
 *            in: path
 *            description: field_id
 *            required: true
 *            schema:
 *              type: integer
 *          - name: id
 *            in: path
 *            description: booking_id
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '200':
 *              description: data Booking yang dipilih berhasil dihapus
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '404':
 *              description: Not Found. Tidak ada data booking dengan id tersebut.
 * 
 * /api/v1/bookings/{id}:
 *  put:
 *      summary: join/unjoin booking
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      parameters:
 *          - name: id
 *            in: path
 *            description: booking_id
 *            schema:
 *              type: integer
 *      responses:
 *          '200':
 *              description: berhasil join/unjoin
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '404':
 *              description: Not Found. Tidak ada data booking dengan id tersebut.
 *      
 * /api/v1/schedule:
 *  get:
 *      summary: get user schedule
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Bookings
 *      responses:
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '200':
 *              description: berhasil mendapat data schedule
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: integer
 *                              name:
 *                                  type: string
 *                              email:
 *                                  type: string
 *                              schedule:
 *                                  $ref: '#definitions/Booking'
 */

export default class BookingsController {
    public async index({params, response, request}: HttpContextContract) {
        if (request.qs().title) {
            let title = request.qs().title
            let indexFiltered = await Booking.findBy('title', title)
            return response.status(200).json({message: 'berhasil mendapat data booking', data: indexFiltered})
        }

        let indexBooking = await Booking.query().where('field_id', params.field_id).select('id', 'title', 'play_date_start', 'play_date_end', 'user_id', 'field_id')
        return response.status(200).json({message: 'berhasil mendapat data booking', data: indexBooking})
    }

    public async store({ request, response, params, auth }: HttpContextContract) {
        try {
            const fieldId = await Field.findByOrFail('id', params.field_id)
            const user = auth!.user
            const validateBooking = await request.validate(CreateBookingValidator)

            const booking = new Booking()
            booking.play_date_start = validateBooking.play_date_start
            booking.play_date_end = validateBooking.play_date_end
            booking.title = validateBooking.title
            booking.userId = user!.id

            booking.related('field').associate(fieldId)
            return response.created({status: 'sukses membuat booking!', data: booking})
        } catch (error) {
            response.badRequest({ errors: error.messages })
        }
    }

    public async show({ params, response }: HttpContextContract) {
        const booking = await Booking.query().where('id', params.id).preload('players', (userQuery) => {
            userQuery.select('id', 'name', 'email')
        }).withCount('players').firstOrFail()
        let rawData = JSON.stringify(booking)
        let countPlayer = {...JSON.parse(rawData), players_count: booking.$extras.players_count}
        return response.ok({ status: 'berhasil mendapat data booking berdasarkan id', data: countPlayer})
    }

    public async update({ request, params, response }: HttpContextContract) {
        const validateBooking = await request.validate(CreateBookingValidator)
        let updatedBooking = await Booking.updateOrCreate({id: params.id, fieldId: params.field_id}, validateBooking)
        return response.ok({message: 'berhasil update data booking', data: updatedBooking})
    }

    public async destroy({ params, response }: HttpContextContract) {
        let destroyBooking = await Booking.findOrFail(params.id)
        destroyBooking.delete()
        response.status(200).json({message: 'data booking yang dipilih berhasil dihapus', deletedData: destroyBooking})

    }

    public async join({response, auth, params}: HttpContextContract) {
        const booking = await Booking.findOrFail(params.id)
        let user = auth.user!
        const isJoin = await Database.from('booking_user').where('booking_id', params.id).where('user_id', user.id).first()
        if (!isJoin) {
            await booking.related('players').attach([user.id])
            return response.ok({ message: 'berhasil join! Jangan lupa catat tanggal mainnya ya. Selamat berolahraga!' })
        } else {
            await booking.related('players').detach([user.id])
            return response.ok({ message: 'berhasil unjoin. Anda bisa join kembali pada jadwal booking ini atau jadwal booking lainnya. Salam olahraga.' })
        }
    }

    public async schedule({auth, response}: HttpContextContract) {
        let user = auth.user!
        let dataSchedule = await User.query()
        .select('id', 'name', 'email').where('id', user.id)
        .preload('schedule', (scheduleQuery) => {
            scheduleQuery.select('id', 'play_date_start', 'play_date_end', 'title', 'field_id').preload('field', (fieldQuery) => {
                fieldQuery.select('name', 'type', 'venue_id').preload('venue', (venueQuery) => {
                    venueQuery.select('id', 'name', 'address', 'phone')
                })
            })
        })
        return response.ok({message: 'berhasil mendapat data schedule anda', data: dataSchedule})
    }
}