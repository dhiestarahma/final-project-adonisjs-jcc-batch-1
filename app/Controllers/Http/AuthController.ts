import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
    /**
     * @swagger
     * /api/v1/register:
     *  post:
     *      tags:
     *          - Authentication
     *      summary: register akun
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www-form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/User'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/User'
     *      responses:
     *          '201':
     *              description: User berhasil melakukan register. Cek email untuk memeriksa kode OTP yang telah dikirim.
     *          '422':
     *              description: Unprocessable entity. Dapat disebabkan request body tidak berhasil divalidasi.
     * 
     * /api/v1/login:
     *  post:
     *      tags:
     *          - Authentication
     *      summary: login akun
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www-form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/Login'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/Login'
     *      responses:
     *          '200':
     *              description: berhasil login
     *          '400':
     *              description: email/pasword salah
     * 
     * /api/v1/verifikasi-otp:
     *  post:
     *      tags:
     *          - Authentication
     *      summary: verifikasi OTP
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www-form-urlencoded:
     *                  schema:
     *                      type: object
     *                      properties:
     *                          otp_code:
     *                              type: string
     *                              description: kode OTP yang diperoleh dari email
     *                          email:
     *                              type: string
     *                              description: email pengguna yang digunakan saat register
     *                      required:
     *                          - otp_code
     *                          - email
     *      responses:
     *          '200':
     *              description: berhasil verifikasi kode OTP
     *          '404':
     *              description: (row not found) Kode OTP yang dimasukkan salah
     *          '400':
     *              description: (Bad Request) Kode OTP benar, email salah
     */
    public async register({ request, response }: HttpContextContract ) {
        try {

            let data = await request.validate(UserValidator)
            let newUser = await User.create(data)

            const otp_code = Math.floor(100000 + Math.random() * 900000)

            await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})
            await Mail.send((message) => {
                message
                    .from('admin@sanberdev.com')
                    .to(newUser.email)
                    .subject('Welcome onboard!')
                    .htmlView('emails/otp_verification', {otp_code})
            })
            return response.created({ message: `Halo ${data.name}. Anda sudah berhasil melakukan register. Silakan cek email anda untuk memeriksa kode OTP yang telah dikirim.`, data: newUser })
        } catch (error) {
            return response.unprocessableEntity({ messages: error.messages})
        }
    }

    public async login({ request, response, auth }: HttpContextContract) {
        try {
            const userSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            
            const email = request.input('email')
            const password = request.input('password')

            await request.validate({ schema: userSchema })
            const token = await auth.use('api').attempt(email, password)

            return response.ok({ message: 'Login berhasil. Selamat datang di aplikasi main bersama.', token })
        } catch (error) {
            if (error.guard) {
                return response.badRequest({ messages: 'login error', error: error.message })
            } else {
                return response.badRequest({ messages: 'login error', error: error.messages })
            }
            
        }
        
    }

    public async otpConfirmation({ request, response }: HttpContextContract) {
        let otp_code = request.input('otp_code')
        let email = request.input('email')

        let user = await User.findBy('email', email)
        let otpCheck = await Database.from('otp_codes').where('otp_code', otp_code).firstOrFail()

        if (user!.id == otpCheck.user_id) {
            user!.isVerified = true
            await user?.save()
            return response.status(200).json({message: 'berhasil verifikasi OTP!'})
        } else {
            return response.status(400).json({ message: 'Maaf anda gagal verifikasi OTP. Silakan cek kembali email anda, dan pastikan input data OTP dengan benar.'})
        }
    }
}
