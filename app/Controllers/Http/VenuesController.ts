import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import Venue from 'App/Models/Venue'

/**
 * @swagger
 * /api/v1/venues:
 *  get:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues
 *      summary: Menampilkan seluruh data Venue
 *      responses:
 *          '200':
 *              description: Sukses menampilkan data venue
 *              content:
 *                  application/json:
 *                      schemas:
 *                          message:
 *                              type: string
 *                              example:
 *                                  message: berhasil mendapat data venue
 *                          data:
 *                              type: array
 *                              items:
 *                                  type: object
 *                                  properties:
 *                                      id: integer
 *                                      name: string
 *                                      address: string
 *                                      phone: string
 *                                  example:
 *                                      - id: 1
 *                                        name: SARAGA ITB
 *                                        address: Jl.Siliwangi Dalam No 3, Bandung
 *                                        phone: +6282112789712
 *                                      - id: 2
 *                                        name: GOR SAHABUDIN
 *                                        address: Jl. Merdeka, Pangkalpinang
 *                                        phone: +6285318185757
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 * 
 *  post:
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues
 *      summary: Menambah data Venue baru
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          name:
 *                              type: string
 *                              description: nama venue
 *                          address:
 *                              type: string
 *                              description: alamat venue
 *                          phone:
 *                              type: string
 *                              description: nomor telepon venue
 *                      required:
 *                          - name
 *                          - address
 *                          - phone
 *      responses:
 *          '201':
 *              description: sukses menambah data venue baru
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '400':
 *              description: request body yang diinput tidak valid.
 * 
 * /api/v1/venues/{id}:
 *  get:
 *      security:
 *          - bearerAuth: []
 *      summary: menampilkan data venue berdasarkan id
 *      tags:
 *          - Venues
 *      parameters:
 *          - name: id
 *            in: path
 *            description: venue id
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '201':
 *              description: berhasil mendapat data venue dengan id
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#definitions/Venue'
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '404':
 *              description: Not Found. Tidak ada data venue dengan id tersebut.
 * 
 *  put:
 *      summary: mengupdate data venue berdasarkan id
 *      security:
 *          - bearerAuth: []
 *      tags:
 *          - Venues
 *      parameters:
 *          - name: id
 *            in: path
 *            description: venue id
 *            required: true
 *            schema:
 *              type: integer
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          name:
 *                              type: string
 *                              description: nama venue
 *                          address:
 *                              type: string
 *                              description: alamat venue
 *                          phone:
 *                              type: string
 *                              description: nomor telepon venue
 *                      required:
 *                          - name
 *                          - address
 *                          - phone
 *      responses:
 *          '200':
 *              description: sukses mengupdate data venue
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '400':
 *              description: request body yang diinput tidak valid.
 *          '404':
 *              description: Not Found. Tidak ada data venue dengan id tersebut.
 * 
 *  delete:
 *      security:
 *          - bearerAuth: []
 *      summary: Menghapus data venue berdasarkan id
 *      tags:
 *          - Venues
 *      parameters:
 *          - name: id
 *            in: path
 *            description: venue id
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          '203':
 *              description: data Venue yang dipilih berhasil dihapus
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '404':
 *              description: Not Found. Tidak ada data venue dengan id tersebut.
 * 
 */
         
export default class VenuesController {
    public async index({ response, request }: HttpContextContract) {
        if (request.qs().name) {
            let name = request.qs().name
            let indexFiltered = await Venue.findBy('name', name)
            return response.status(200).json({ messange: 'berhasil mendapatkan data Venue!', data: indexFiltered})
        }
        
        let indexVenue = await Venue.all()
        return response.status(200).json({ messange: 'berhasil mendapatkan data Venue!', data: indexVenue})
    }

    public async store({ request, response }: HttpContextContract) {
        try {
            await request.validate(CreateVenueValidator)

            let newVenues = new Venue()
            newVenues.name = request.input('name')
            newVenues.address = request.input('address')
            newVenues.phone = request.input('phone')

            await newVenues.save()
            response.created({ message: 'berhasil input data venue baru!', newId: newVenues.id })
        } catch (error) {
            response.badRequest({ errors: error.messages })
        }
    }

    public async show({ params, response }: HttpContextContract) {
        let showVenue = await Venue.query().select('id', 'name', 'address', 'phone')
            .where('id', params.id)
            .preload('fields', (fieldQuery) => {
                fieldQuery.select('id', 'name', 'type')
            })
            .firstOrFail()
        response.status(201).json({ message: 'berhasil mendapat data venue dengan id', data: showVenue })
    }

    public async update({ request, response, params }: HttpContextContract) {
        const validateVenue = await request.validate(CreateVenueValidator)
        let updatedVenue = await Venue.updateOrCreate({id: params.id}, validateVenue)
        return response.ok({message: 'berhasil update data venue', data: updatedVenue})
    }

    public async destroy({ params, response }: HttpContextContract) {
        let deleteVenue = await Venue.findOrFail(params.id)
        deleteVenue.delete()
        response.status(203).json({ message: 'data Venue yang dipilih berhasil dihapus', data: deleteVenue })
    }
}