import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'


/**
 * @swagger
 * /api/v1/venues/{venue_id}/fields/:
 *  get:
 *    security:
 *      - bearerAuth: []
 *    summary: Menampilkan seluruh data Field
 *    tags:
 *      - Fields
 *    parameters:
 *      - name: venue_id
 *        in: path
 *        description: venue_id
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      '200':
 *        description: berhasil mendapatkan data Field
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#definitions/Field'
 *      '401':
 *        description: Unauthorized. Butuh token yang didapat saat login.
 * 
 *  post:
 *    security:
 *      - bearerAuth: []
 *    summary: Menambah data field baru
 *    tags:
 *      - Fields
 *    parameters:
 *      - name: venue_id
 *        in: path
 *        description: venue_id
 *        required: true
 *        schema:
 *          type: integer
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *                description: nama field/lapangan
 *              type:
 *                type: string
 *                enum: [futsal, minisoccer, basketball, volleyball, soccer]
 *                description: jenis lapangan (futsal, minisoccer, basketball, volleyball, soccer)
 *            required:
 *              - name
 *              - type
 *    responses:
 *          '201':
 *              description: sukses menambah data field/lapangan baru
 *          '401':
 *              description: Unauthorized. Butuh token yang didapat saat login.
 *          '400':
 *              description: request body yang diinput tidak valid.
 * 
 * /api/v1/venues/{venue_id}/fields/{id}:
 *  get:
 *    summary: Menampilkan data Field berdasarkan id
 *    tags:
 *      - Fields
 *    security:
 *      - bearerAuth: []
 *    parameters:
 *      - name: venue_id
 *        in: path
 *        description: venue_id
 *        required: true
 *        schema:
 *          type: integer
 *      - name: id
 *        in: path
 *        description: field_id
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      '201':
 *        description: berhasil mendapat data field berdasarkan id
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#definitions/Field'
 *      '401':
 *        description: Unauthorized. Butuh token yang didapat saat login.
 *      '404':
 *        description: Not Found. Tidak ada data venue dengan id tersebut.
 *        
 *  put:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Fields
 *    summary: mengupdate data field berdasarkan id
 *    parameters:
 *      - name: venue_id
 *        in: path
 *        description: venue_id
 *        required: true
 *        schema:
 *          type: integer
 *      - name: id
 *        in: path
 *        description: field_id
 *        required: true
 *        schema:
 *          type: integer
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *                description: nama lapangan/field
 *              type:
 *                type: string
 *                enum: [futsal, minisoccer, basketball, volleyball, soccer]
 *                description: jenis lapangan (futsal, minisoccer, basketball, volleyball, soccer)
 *            required:
 *              - name
 *              - type
 *    responses:
 *      '202':
 *        description: sukses mengupdate data field/lapangan
 *      '401':
 *        description: Unauthorized. Butuh token yang didapat saat login.
 *      '400':
 *        description: request body yang diinput tidak valid.
 *      '404':
 *        description: Not Found. Tidak ada data field dengan id tersebut.
 * 
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    summary: Menghapus data field berdasarkan id
 *    tags:
 *      - Fields
 *    parameters:
 *      - name: venue_id
 *        in: path
 *        description: venue_id
 *        required: true
 *        schema:
 *          type: integer
 *      - name: id
 *        in: path
 *        description: field_id
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      '203':
 *        description: data Venue yang dipilih berhasil dihapus
 *      '401':
 *        description: Unauthorized. Butuh token yang didapat saat login.
 *      '404':
 *        description: Not Found. Tidak ada data venue dengan id tersebut.
 */
export default class FieldsController {
  public async index ({ params, response, request }: HttpContextContract) {

    if (request.qs().name) {
      let name = request.qs().name
      let indexFiltered = await Field.findBy('name', name)
      return response.status(200).json({ messange: 'berhasil mendapatkan data Field!', data: indexFiltered})
    }
  
  let indexField = await Field.query().where('venue_id', params.venue_id)
  return response.status(200).json({ messange: 'berhasil mendapatkan data Field!', data: indexField})
  }

  public async store ({ params, request, response }: HttpContextContract) {
    try {
      await request.validate(CreateFieldValidator)

      const venue = await Venue.findByOrFail('id', params.venue_id)
      let newField = new Field()
      newField.name = request.input('name')
      newField.type = request.input('type')

      await newField.related('venue').associate(venue)
      response.created({ message: 'berhasil input data field baru!', newId: newField.id })
  } catch (error) {
      response.badRequest({ errors: error.message })
  }
  }

  public async show ({ params, response }: HttpContextContract) {
    let showField = await Field.query()
                    .select('id', 'name', 'type', 'venue_id')
                    .where('venue_id', params.venue_id)
                    .andWhere('id', params.id)
                    .preload('venue', (venueQuery) => {
                      venueQuery.select('name', 'address', 'phone', 'id')
                    })
                    .preload('bookings', (bookingQuery) => {
                      bookingQuery.select('play_date_start', 'play_date_end', 'id')
                    })
                    .firstOrFail()
    response.status(201).json({ message: 'berhasil mendapat data fields dengan id', data: showField })
  }

  public async update ({ request, response, params }: HttpContextContract) {
    await request.validate(CreateFieldValidator)
    let updateField = await Field.findOrFail(params.id)
    updateField.name = request.input('name')
    updateField.type = request.input('type')
    updateField.venueId = params.venue_id
    updateField.save()

    response.status(202).json({ message: 'data Field berhasil di-update!' , data: updateField})
  }

  public async destroy ({ params, response }: HttpContextContract) {
    let destroyField = await Field.findOrFail(params.id)
    destroyField.delete()
    response.status(203).json({ message: 'data Field yang dipilih berhasil dihapus', data: destroyField })
  }
}
