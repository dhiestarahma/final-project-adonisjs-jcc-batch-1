import { DateTime } from 'luxon'
import { 
  BaseModel, 
  BelongsTo, 
  belongsTo, 
  column,
  HasMany,
  hasMany, 
} from '@ioc:Adonis/Lucid/Orm'

import Venue from './Venue'
import Booking from './Booking'

/**
 * @swagger
 * definitions:
 *  Field:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *      name:
 *        type: string
 *      type:
 *        type: string
 *        enum: [futsal, minisoccer, basketball, volleyball, soccer]
 *      venue_id:
 *        type: number
 *      venue:
 *        $ref: '#definitions/Venue'
 *      bookings:
 *        $ref: '#definitions/Booking'
 *    required:
 *      - name
 *      - type
 */
export default class Field extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public type: 'futsal' | 'minisoccer' | 'basketball' | 'volleyball' | 'soccer'

  @column()
  public venueId: number

  @column.dateTime({ autoCreate: true,
      serialize: (value: DateTime | null) => {
        return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
      } 
  })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }  
  })
  public updatedAt: DateTime

  @belongsTo(() => Venue)
  public venue: BelongsTo<typeof Venue>

  @hasMany(() => Booking)
  public bookings: HasMany<typeof Booking>
}
