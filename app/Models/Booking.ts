import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import Field from './Field'

/**
 * @swagger
 * definitions:
 *  Booking:
 *    type: object
 *    properties:
 *      id:
 *        type: number
 *      play_date_start:
 *        type: string
 *        format: date-time
 *      play_date_end:
 *        type: string
 *        format: date-time
 *      title:
 *        type: string
 *      field_id:
 *        type: integer
 *      user_id:
 *        type: integer
 *      field:
 *        $ref: '#definitions/Field'
 */
export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }
  })
  public play_date_start: DateTime

  @column.dateTime({
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }
  })
  public play_date_end: DateTime

  @column()
  public title: string

  @column()
  public fieldId: number

  @column()
  public userId: number

  @column.dateTime({ 
    autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }
  })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }
  })
  public updatedAt: DateTime

  @manyToMany(() => User)
  public players: ManyToMany<typeof User>

  @belongsTo(() => Field)
  public field: BelongsTo<typeof Field>

  @belongsTo(() => User)
  public bookingUser: BelongsTo<typeof User>
}
