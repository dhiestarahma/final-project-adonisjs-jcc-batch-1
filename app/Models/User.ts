import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
  manyToMany,
  ManyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Booking from './Booking'

/**
 * @swagger
 * definitions:
 *  User:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *      name:
 *        type: string
 *        description: nama pengguna (minimal 3 huruf)
 *      email:
 *        type: string
 *        description: email pengguna (harus unik/tidak boleh sama dengan pengguna lain)
 *      password:
 *        type: string
 *        description: password minimal 8 karakter
 *      role:
 *        type: string
 *        enum: [user, owner]
 *        description: role hanya ada 2, user dan owner. Pilih salah satu.
 *    required:
 *      - name
 *      - email
 *      - password
 *      - role
 *  Login:
 *    type: object
 *    properties:
 *      email:
 *        type: string
 *        description: email pengguna
 *      password:
 *        type: string
 *        description: password pengguna
 *    required:
 *      - email
 *      - password
 */

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string
  
  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public isVerified: boolean

  @column()
  public role: 'user' | 'owner'

  @column.dateTime({ autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    } 
  })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }  
  })
  public expiresAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @hasMany(() => Booking)
  public booking: HasMany<typeof Booking>

  @manyToMany(() => Booking)
  public schedule: ManyToMany<typeof Booking>
}
