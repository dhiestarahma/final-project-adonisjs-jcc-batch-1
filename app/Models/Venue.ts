import { DateTime } from 'luxon'
import { 
  BaseModel, 
  column,
  hasMany,
  HasMany 
} from '@ioc:Adonis/Lucid/Orm'

import Field from './Field'

/**
 * @swagger
 * definitions:
 *  Venue:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *      name:
 *        type: string
 *      address:
 *        type: string
 *      phone:
 *        type: string
 *      field:
 *        $ref: '#definitions/Field'
 *    required:
 *      - name
 *      - address
 *      - phone
 */
export default class Venue extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public address: string

  @column()
  public phone: string

  @column.dateTime({ autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    } 
  })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.setZone('utc', {keepLocalTime: true}).toFormat('dd LLL yyyy hh:mm:ss') : value;
    }  
  })
  public updatedAt: DateTime

  @hasMany(() => Field)
  public fields: HasMany<typeof Field>
}
