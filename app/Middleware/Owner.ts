import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class VenueOwner {
  public async handle ({ auth, response }: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let isVerified = auth.user?.isVerified
    let userRole = auth.user?.role

    if (isVerified) {
      if (userRole == 'owner') {
      await next()
      } else {
        return response.unauthorized({ message: 'hanya owner yang memiliki akses' })
      }
    } else {
      return response.unauthorized({message: 'akun belum terverifikasi. Harap cek email anda dan melakukan verifikasi akun terlebih dahulu'})
    }
  }
}
