import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateFieldValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
	name: schema.string({}, [
		rules.alpha({
			allow: ['space']
		}),
		rules.minLength(3)
	]),
	type: schema.enum(
		['futsal', 'minisoccer', 'basketball', 'volleyball', 'soccer'] as const
		),
  })

  public messages = {
	'required': 'Untuk membuat field/lapangan baru, wajib memasukkan data {{field}}',
	'name.alpha': 'Data {{field}} hanya boleh mengandung huruf tanpa simbol ataupun angka',
	'name.minLength': 'Data {{field}} minimal terdiri atas 3 huruf',
	'type.enum': 'lapangan yang ada hanya futsal, soccer, minisoccer, volleyball, dan basketball'
  }
}
