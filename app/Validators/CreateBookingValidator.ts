import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateBookingValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
	play_date_start: schema.date({}, [
		rules.after('today')
	]),
	play_date_end: schema.date({}, [
		rules.afterOrEqualToField('play_date_start')
	]),
	title: schema.string({}, [
		rules.minLength(3)
	])
  })

  public messages = {
	'required': 'Untuk melakukan booking, wajib memasukkan data {{field}}',
	'play_date_start.date.format': 'format tanggal booking YYYY-MM-DD atau jika ingin mencantumkan waktu(jam) maka formatnya YYYY-MM-DDThh:mm:ss',
	'play_date_end.date.format': 'format tanggal booking YYYY-MM-DD atau jika ingin mencantumkan waktu(jam) maka formatnya YYYY-MM-DDThh:mm:ss',
	'play_date_start.after': 'unavailabe: minimal booking h-1',
	'play_date_end.after': 'unavailabe: waktu berakhir booking harus setelah booking dimulai',
	'title.minLength': 'judul booking minimal terdiri dari 3 karakter'
  }
}
