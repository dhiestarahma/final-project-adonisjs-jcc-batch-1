import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class UserValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
	  name: schema.string({}, [
		  rules.minLength(3)
	  ]),
	  email: schema.string({}, [
		  rules.email(),
		  rules.unique({table: 'users', column: 'email'})
	  ]),
	  password: schema.string({}, [
		  rules.minLength(8)
	  ]),
	  role: schema.enum(
		  ['user', 'owner'] as const
	  )
  })

  public messages = {
	'required': 'Untuk membuat venue baru, wajib memasukkan data {{field}}',
	'name.minLength': 'Data {{field}} minimal terdiri atas 3 huruf',
	'email.email': 'alamat email tidak valid',
	'email.unique': 'alamat email sudah digunakan oleh user lain, silakan masukkan alamat email yang lain',
	'role.enum': 'role yang tersedia hanya user dan owner. Jika anda ingin melakukan booking dan join/unjoin, pilih user. Jika anda admin/owner venue, silakan pilih role sebagai owner.'
  }
}
