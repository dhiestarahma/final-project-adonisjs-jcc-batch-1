import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateVenueValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
	name: schema.string({}, [
		rules.alpha({
			allow: ['space']
		}),
		rules.minLength(3)
	]),
	address: schema.string({}, [
		rules.minLength(5)
	]),
	phone: schema.string({}, [
		rules.mobile({ strict: true})
	])
  })

  public messages = {
	'required': 'Untuk membuat venue baru, wajib memasukkan data {{field}}',
	'name.alpha': 'Data {{field}} hanya boleh mengandung huruf tanpa simbol ataupun angka',
	'name.minLength': 'Data {{field}} minimal terdiri atas 3 huruf',
	'phone.mobile': 'Data {{field}} harus diawali dengan tanda + dan diikuti dengan kode negara dan nomor telepon (contoh: +628xxxxxxxxxx)'
  }
}
