/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
Route.get('/', async () => {
  return { hello: 'world' }
}).as('home')
Route.post('/register', 'AuthController.register').as('auth.register'),
Route.post('/login', 'AuthController.login').as('auth.login'),
Route.post('/verifikasi-otp', 'AuthController.otpConfirmation').as('auth.otpVerify')
Route.resource('venues', 'VenuesController').apiOnly().middleware({
  'store': ['auth', 'owner'],
  'update': ['auth', 'owner'],
  'destroy': ['auth', 'owner'],
  'index': ['auth'],
  'show': ['auth']
}),
Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
  'store': ['auth', 'owner'],
  'update': ['auth', 'owner'],
  'destroy': ['auth', 'owner'],
  'index': ['auth'],
  'show': ['auth']
}),
Route.resource('fields.bookings', 'BookingsController').apiOnly().middleware({
  'store': ['auth', 'user'],
  'update': ['auth', 'user'],
  'destroy': ['auth', 'user'],
  'index': ['auth'],
  'show': ['auth']
}),
Route.put('/bookings/:id', 'BookingsController.join').as('booking.joinUnjoin')
  .middleware('auth')
  .middleware('user'),
Route.get('/schedule', 'BookingsController.schedule').as('user.schedule')
  .middleware('auth')
  .middleware('user')
}).prefix('/api/v1')